<?php

function opr2_ibs_bundle_register_ibs() {
  $bundle = array(
    'machine_name' => 'register_ibs',
    'instances' => array(
      'field_form_version' => array(
        'default_value' => array(array('value'=>'v1')),
      ),
    ), // end instances

    'groups' => array(
    ), // end groups

    'tree' => array(
      'field_form_version',

      'field_inpatient',

      'group_inline' => array(
      ),
    ), // end tree
  );

  return $bundle;
}
